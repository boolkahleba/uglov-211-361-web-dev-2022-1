<!DOCTYPE html>
<html lang="en">
<head>
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <!--    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&family=Noto+Sans:wght@300&display=swap"-->
    <!--          rel="stylesheet">-->
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>Uglov 211-361 lab3</title>
</head>
<body>
<header style="position:fixed; top:0px; left:0px; right:0px;">
    <div>
        <nav>
            <?php
            $name='Боевики';
            $link='index.php';
            $current_page=false;
            echo '<a href="'.$link.'"';
            ?><?php
            if( $current_page )
                echo 'class="selected_menu"';
            echo '>'.$name.'</a>';
            ?>
            <?php
            $name='Комедии';
            $link='page2.php';
            $current_page=true;
            echo '<a style = "padding-left:5em" href="'.$link.'"';
            ?><?php
                if( $current_page )
                    echo 'class="selected_menu"';
                echo '>'.$name.'</a>';
                ?>
            <?php
            $name='Драмы';
            $link='page3.php';
            $current_page=false;
            echo '<a style="padding-left:5em" href="'.$link.'"';
            ?><?php
            if( $current_page )
                echo 'class="selected_menu"';
            echo '>'.$name.'</a>';
            ?>
        </nav>
    </div>
</header>
<main style="text-align: center">
    <h1>Комедии</h1>

    <?php
    $cat = 'images/котик.jpg';
    $s = date('s');
    $os = $s % 2;

    if( $os === 0 ) {
        $uboinie_kanikuli = 'images/uboinie_kanikuli.jpg';
        $spy = 'images/spy.jpg';
    }
    else {
        $uboinie_kanikuli = $cat;
        $spy = $cat;
    }
    ?>

    <h2>Убойные каникулы</h2>
    <div style="display: flex; justify-content: center">
        <ul style="text-align: left">
            <?php
            $mass = ["Комедия, пародия, ужасы.", "Канада, США, Индия, Великобритания, 2010 год.", "Длительность: 85 минут."];
            foreach ($mass as $item) {
                echo '<li>' .$item. '</li>';
            }
            ?>
        </ul>
    </div>
    <div>
        <img alt="Убойные каникулы" src="<?php echo ($uboinie_kanikuli);?>" style="width: 17em;" class="image-in-text">
    </div>
    <div style="display: flex; justify-content: center; padding-bottom: 1em">
        <div class="text-block">
            <p>
                Группа студентов колледжа отправляется за город, чтобы вовсю оторваться на выходных: попить пивка,
                поплавать в озере и подышать чистым лесным воздухом. Но уже в первый день они сталкиваются с
                неотесанными фермерами Такером и Дэйлом, также решившими как следует оттянуться на природе. Приняв этих
                диковатых, но, в сущности, симпатичных ребят за «злобных местных», горожане решают дать деревенщине
                жесткий отпор. Но с каждым их шагом становится только хуже…
            </p>
        </div>
    </div>

    <h2>Шпион</h2>
    <div style="display: flex; justify-content: center; padding-bottom: 1em">
        <ul style="text-align: left">
            <li>Комедия, боевик.</li>
            <li>США, 2015 год.</li>
            <li>Длительность: 119 минут.</li>
        </ul>
    </div>
    <div>
        <img alt="Шпион" src="<?php echo ($spy);?>" style="width: 17em;" class="image-in-text">
    </div>
    <div style="display: flex; justify-content: center; padding-bottom: 1em">
        <div class="text-block">
            <p>
                Сьюзан Купер всю свою жизнь мечтала стать секретным агентом и даже устроилась работать в ЦРУ. Однако,
                дальше сотрудника самой низкой ступени ей так и не удалось продвинуться. Понимая, что осуществить свою
                мечту практически невозможно, она все-таки продолжает надеяться на лучшее и с нетерпением ожидает своего
                шанса. И вскоре судьба дарит ей такую возможность. Лучший агент проваливает секретное задание, в ходе
                которого он должен был выяснить местонахождение ядерной бомбы, но, совершенно случайно, застрелил
                единственного человека, владеющего этой информацией.Через некоторое время службе разведки становится
                известно, что Рейна Боянова знает, где находится бомба, но она также знает в лицо всех секретных агентов
                ЦРУ. Теперь у руководства не остается другого выбора, как воспользоваться услугами Сьюзан Купер, но
                сможет ли она справиться со столь сложной миссией?..
            </p>
        </div>
    </div>
</main>
</body>
<footer>
    <div id="contacts">
        <a href="https://t.me/Boolka_hlebaa" target="_blank" target="_blank"><i class="fa-brands fa-telegram"></i></a>
        <a href="https://vk.com/boolka_hleba" target="_blank" target="_blank"><i class="fa-brands fa-vk"></i></a>
        <a style="font-size: 0.5em; color: black; text-decoration: none" href="tel: +7(923)613-90-55">+7 (923)
            613-90-55</a>
        <?php
        date_default_timezone_set("Europe/Moscow");
        echo '<p style="font-size: 0.7em">Сформировано '.date('d.m.Y').' в '.date('H-i:s').'</p>';
        ?>
    </div>
</footer>
</html>