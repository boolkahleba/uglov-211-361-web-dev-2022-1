<!DOCTYPE html>
<html lang="en">
<head>
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <!--    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&family=Noto+Sans:wght@300&display=swap"-->
    <!--          rel="stylesheet">-->
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>Uglov 211-361 lab3</title>
</head>
<body>
<header style="position:fixed; top:0px; left:0px; right:0px;">
    <div>
        <nav>
            <?php
            $name='Боевики';
            $link='index.php';
            $current_page=false;
            echo '<a href="'.$link.'"';
            ?><?php
            if( $current_page )
                echo 'class="selected_menu"';
            echo '>'.$name.'</a>';
            ?>
            <?php
            $name='Комедии';
            $link='page2.php';
            $current_page=false;
            echo '<a style = "padding-left:5em" href="'.$link.'"';
            ?><?php
            if( $current_page )
                echo 'class="selected_menu"';
            echo '>'.$name.'</a>';
            ?>
            <?php
            $name='Драмы';
            $link='page3.php';
            $current_page=true;
            echo '<a style="padding-left:5em" href="'.$link.'"';
            ?><?php
            if( $current_page )
                echo 'class="selected_menu"';
            echo '>'.$name.'</a>';
            ?>
        </nav>
    </div>
</header>
<main style="text-align: center">
    <h1>Драмы</h1>

    <?php
    $cat = 'images/котик.jpg';
    $s = date('s');
    $os = $s % 2;

    if( $os === 0 ) {
        $one = 'images/1+1.jpg';
        $hachiko = 'images/hachiko.jfif';
    }
    else {
        $one = $cat;
        $hachiko = $cat;
    }
    ?>

    <h2>1+1</h2>
    <div style="display: flex; justify-content: center; padding-bottom: 1em">
        <ul style="text-align: left">
            <li>Комедия, драма, биография.</li>
            <li>Франция, 2011 год.</li>
            <li>Длительность: 112 минут.</li>
        </ul>
    </div>
    <div>
        <img alt="1+1" src="<?php echo ($one)?>" style="width: 17em;" class="image-in-text">
    </div>
    <div style="display: flex; justify-content: center">
        <div class="text-block">
            <p>
                Пострадав в результате несчастного случая, богатый аристократ Филипп нанимает в помощники
                человека, который менее всего подходит для этой работы, - молодого жителя предместья Дрисса, только что
                освободившегося из тюрьмы. Несмотря на то, что Филипп прикован к инвалидному креслу, Дриссу удается
                привнести в размеренную жизнь аристократа дух приключений.
            </p>
        </div>
    </div>

    <h2>Хатико</h2>
    <div style="display: flex; justify-content: center">
        <ul style="text-align: left">
            <li>Драма.</li>
            <li>США, 2009 год.</li>
            <li>Длительность: 93 минуты.</li>
        </ul>
    </div>
    <div>
        <img alt="Хатико" src="<?php echo ($hachiko)?>" style="width: 17em;" class="image-in-text">
    </div>
    <div style="display: flex; justify-content: center; padding-bottom: 1em">
        <div class="text-block">
            <p>
                В основе сюжета — реальная история, случившаяся в Японии и потрясшая весь мир. Однажды, возвращаясь с
                работы, профессор колледжа нашел на вокзале симпатичного щенка породы акита-ину. Профессор и Хатико
                стали верными друзьями. Каждый день пес провожал и встречал Профессора на вокзале. И даже потеря хозяина
                не остановила пса в его надежде, что друг вернется.
            </p>
        </div>
    </div>
</main>
</body>
<footer>
    <div id="contacts">
        <a href="https://t.me/Boolka_hlebaa" target="_blank" target="_blank"><i class="fa-brands fa-telegram"></i></a>
        <a href="https://vk.com/boolka_hleba" target="_blank" target="_blank"><i class="fa-brands fa-vk"></i></a>
        <a style="font-size: 0.5em; color: black; text-decoration: none" href="tel: +7(923)613-90-55">+7 (923)
            613-90-55</a>
        <?php
        date_default_timezone_set("Europe/Moscow");
        echo '<p style="font-size: 0.7em">Сформировано '.date('d.m.Y').' в '.date('H-i:s').'</p>';
        ?>
    </div>
</footer>
</html>