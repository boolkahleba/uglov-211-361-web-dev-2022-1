<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Таблица умножения</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
<?php
$type = "table";
$table = "all";
if (isset($_GET['table'])) {
    $table = $_GET['table'];
}

if (isset($_GET['type'])) {
    $type = $_GET['type'];
}
?>
<header>
    <nav style="text-align: center">
                <a <?php if ($_GET['type'] == 'table') {
                    echo 'class="selected"';
                }?> href="?type=table&table=all">Табличная вёрстка</a></li>

                <a style="padding-left: 2em" <?php if ($type == 'block') echo 'class="selected"'?> href="?type=block&table=all">Блочная вёрстка</a></li>

    </nav>
</header>
<main style="display: flex; flex-flow: row">
    <menu class="left">

        <a <?php
        if ($table == 'all') echo 'class="selected"'?> href="?table=all&type=<?php echo $type; ?>">Всё</a>
        <?php
        for ($i = 2; $i <= 10; $i++) {
            $selected = "";
            if ($table == $i)
                $selected = 'class="selected"';
            echo '<a '.$selected.' href="?table='.$i.'&type='.$type.'">'.$i.'</a>';
        }

        function one_row_table($table, $type): void
        {
            echo '<table class="table" style="">';
            echo '<tr style="padding-left: 0">';
            for ($i = 2; $i <= 10; $i++) {
                echo '<td style="padding-left: 1em"><a href="?table=' . $i . '&type=' . $type . '">' . $i . '</a></td>';
            }
            echo '</tr>';
            echo '<tr>';
            for ($i = 2; $i <= 10; $i++) {
                echo '<td style="padding-left: 1em"><a href="?table='.$i.'&type='.$type.'">' . $i . '</a>*<a href="?table='.$table.'&type='.$type.'">' . $table . '</a>='.($table * $i <= 10 ? '<a href="?table='.$i*$table.'&type='.$type.'">' . $i*$table . '</a>': $table*$i).'</td>';
            }
            echo '</tr>';
            echo '</table>';
        }
        function one_row_block_table($table, $type): void {
            echo '<div class="block_table">';
            for ($i = 2; $i <= 10; $i++) {
                echo '<div class="row">';
                echo '<div class="col"><a href="?table='.$i.'&type='.$type.'">' . $i . '</a></div>';
                echo '<div class="col"><p><a href="?table='.$i.'&type='.$type.'">' . $i . '</a>*<a href="?table='.$table.'&type='.$type.'">' . $table . '</a>='.($table * $i <= 10 ? '<a href="?table='.$i*$table.'&type='.$type.'">' . $i*$table . '</a>': $table*$i).'</p></div>';
                echo '</div>';
            }
            echo '</div>';
        }
        ?>
    </menu>
    <section>
        <?php
        function render_all($type): void
        {
            switch ($type) {
                case 'table':
                    echo '<table class="table">';
                    for ($i = 1; $i <= 10; $i++) {
                        echo '<tr>';
                        for ($j = 1; $j <= 10; $j++) {
                            echo '<td><p><a href="?table=' . $i . '&type=' . $type . '">' . $i . '</a>*<a href="?table=' . $j . '&type=' . $type . '">' . $j . '</a>=' . ($j * $i <= 10 ? '<a href="?table=' . $i * $j . '&type=' . $type . '">' . $i * $j . '</a>' : $j * $i) . '</p></td>';
                        }
                        echo '</tr>';
                    }
                    echo '</table>';
                    break;
                case 'block':
                    echo '<div class="block_table">';
                    for ($i = 1; $i <= 10; $i++) {
                        echo '<div class="row">';
                        for ($j = 1; $j <= 10; $j++) {
                            echo '<div class="col"><p><a href="?table='.$i.'&type='.$type.'">' . $i . '</a>*<a href="?table='.$j.'&type='.$type.'">' . $j . '</a>='.($j * $i <= 10 ? '<a href="?table='.$i*$j.'&type='.$type.'">' . $i*$j . '</a>': $j*$i).'</p></div>';
                        }
                        echo '</div>';
                    }
                    echo '</div>';
                    break;
            };
        }

        function render_one_row($table, $type): void
        {
            switch($type) {
                case 'table':
                    one_row_table($table, $type);
                    break;
                case 'block':
                    one_row_block_table($table, $type);
                    break;
            };
        }
        if ($table == 'all') {
            render_all($type);
        } else {
            render_one_row($table, $type);
        }
        ?>
    </section>
</main>
<footer>
    <?php
    if ($type=="table"){
        echo 'Табличная вёрстка';
    }else{
        echo 'Блочная вёрстка';
    }
    ?>
    <br>
    <?php
    if ($table=="all"){
        echo 'Таблица умножения';
    }else{
        echo 'Таблица умножения на '.$table;
    }
    ?>
    <br>
        <?php echo 'Сформировано '.date('d.m.Y').' в '.date('H:i:s') ?>

</footer>
</body>
</html>