<!DOCTYPE html>
<html lang="en">
<head>
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>SweetShop</title>
</head>
<body>
<header>
    <nav id="#top">
        <div style="text-align: left; padding-left: 2em">
            <img src="images/619535.png" style="height: 1.5em">
        </div>
        <div style="text-align: center">
            <?php
            $name = 'Главная';
            $link = 'index.php';
            $current_page = true;
            echo '<a href="' . $link . '"';

            if ($current_page)
                echo 'class="selected_menu"';
            echo '>' . $name . '</a>';

            $name = 'Магазин';
            $link = 'shop.php';
            $current_page = false;
            echo '<a style = "padding-left:5em" href="' . $link . '"';

            if ($current_page)
                echo 'class="selected_menu"';
            echo '>' . $name . '</a>';

            ?>
        </div>
    </nav>
</header>
<main>
    <div>
        <h1 style="text-align: center; margin-top: 0">
            SweetShop
        </h1>
    </div>

    <h2 style="text-align: center; padding-bottom: 0; margin-bottom: 0">Наши преимущества:</h2>
    <div style="display: flex; justify-content: center; padding-bottom: 1em">
        <ul style="text-align: left">
            <li>Более 10 лет на рынке</li>
            <li>Товары из более чем 20 стран мира</li>
            <li>Только натуральная продукция</li>
        </ul>
    </div>

    <h2 style="text-align: center">О магазине:</h2>
    <div style="display: flex; justify-content: center; padding-bottom: 1em">
        <div class="text-block">
        <p>
            Мы специализируемся на поставках уникальных, редких и оооочень нестандартных сладостей со всего мира. В
            нашем магазине, ты сможешь найти эксклюзивные вкусняшки, не представленные в магазинах России,
            лимитированные выпуски от мировых брендов и очень необычные сладости и не только!
        </p>
        <br>
        <p>
            Мы подобрали самые интересные решения для подарков на любой случай. Мы заботимся о качестве нашей продукции,
            мы постоянно расширяем наш ассортимент и ищем новых поставщиков самых удивительных сладких товаров.
        </p>
        <br>
        <p>
            Мы сделали наш магазин максимально удобным для Вас. Широчайший ассортимент, доступные цены, отличный сервис,
            быстрая доставка по всей России и гарантия качества всей продукции, продаваемой на нашем сайте. Это и есть
            залог успеха нашего магазина.
        </p>
        <br>
        <p>
            Мы беспокоимся о том, чтобы Вам действительно нравился наш магазин и Вы возвращались к нам снова и снова.
            Поэтому мы рады вашим письмам с предложениями и отзывам о нашей работе. Все это позволяет нам становится с
            каждым днём все лучше.
        </p>
    </div>
    </div>
    <div style="display: flex; justify-content: center; padding-bottom: 2em">
        <a href="#top" style="color: #222222; text-decoration: none">
            <div class="button">
                <p>Подняться наверх</p>
            </div>
        </a>
    </div>

</main>
</body>
<footer>
    <div id="contacts">
        <a href=""><i class="fa-solid fa-envelope" style="color: darkorange"></i></a>
        <a href="" target="_blank"><i class="fa-brands fa-telegram"></i></a>
        <a href="" target="_blank"><i class="fa-brands fa-vk"></i></a>
        <a style="font-size: 0.5em; color: black; text-decoration: none" href="tel: +7(999)999-99-99">+7 (999)
            999-99-99</a>
    </div>
</footer>
</html>