<!DOCTYPE html>
<html lang="en">
<head>
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>SweetShop</title>
</head>
<body>
<header>
    <nav id="#top">
        <div style="text-align: left; padding-left: 2em">
            <img src="images/619535.png" style="height: 1.5em">
        </div>
        <div style="text-align: center">
            <?php
            $link = 'index.php';
            $current_page = false;
            echo '<a href="' . $link . '"';

            if ($current_page)
                echo 'class="selected_menu"';
            echo '>Главная</a>';

            $link = 'shop.php';
            $current_page = true;
            echo '<a style = "padding-left:5em" href="' . $link . '"';

            if ($current_page)
                echo 'class="selected_menu"';
            echo '> Магазин</a>';

            include "db.php";
            $result = mysqli_query($mysql, "SELECT * FROM `images`");
            ?>
        </div>
    </nav>
</header>
<main>
    <div>
        <h1 style="text-align: center; margin-top: 0">
            Каталог
        </h1>
    </div>

    <div style="display: flex; justify-content: center">
        <table>
            <tr>
                <?php
                $i = 0;
                while ($name = mysqli_fetch_assoc($result)) {
                    if ($i % 2 == 0 && $i != 0) {
                        echo "</tr><tr>";
                    }
                    ?>
                    <td>
                        <a href="good.php?good_id=<?php echo $name['id'] ?>">
                            <img title="<?php echo $name['name']; ?>" src="images/<?php echo $name['href']; ?>"
                                 style="width: 12em; height: 12em"/>
                            <p style="text-align: center"><?php echo $name['name']; ?></p>
                            <p style="text-align: center; padding-bottom: 25px">Цена: <?php echo $name['coast']; ?> руб.</p>
                        </a>
                    </td>
                    <?php
                    $i += 1;
                }
                ?>
            </tr>
        </table>

    </div>


    <div style="display: flex; justify-content: center; padding-bottom: 2em">
        <a href="#top" style="color: #222222; text-decoration: none">
            <div class="button">
                <p>Подняться наверх</p>
            </div>
        </a>
    </div>

</main>
</body>
<footer>
    <div id="contacts">
        <a href=""><i class="fa-solid fa-envelope" style="color: darkorange"></i></a>
        <a href="" target="_blank"><i class="fa-brands fa-telegram"></i></a>
        <a href="" target="_blank"><i class="fa-brands fa-vk"></i></a>
        <a style="font-size: 0.5em; color: black; text-decoration: none" href="tel: +7(999)999-99-99">+7 (999)
            999-99-99</a>
    </div>
</footer>
</html>