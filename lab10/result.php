<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>form</title>
    <link href="style.css" rel="stylesheet">
</head>
<body>
<main style="padding-top: 1em">

    <div style="display: flex; justify-content: center; font-size: 0.9em; font-style: italic">
        <?php

        if ($_POST["text"] != "") {
            echo '<div class="text-block" style="width: 60%">' . $_POST["text"] . '</div>';
        } else {
            echo 'Нет текста для анализа';
        }
        $text = $_POST["text"];
        $text .= " ";
        ?>
    </div>
    <div style=" display: flex; justify-content: center;">
        <div>
            <?php
            function test_it($text)
            {
                // количество символов в тексте определяется функцией размера текста
                echo 'Количество символов: ' . strlen($text)-1 . '<br>';
                // определяем ассоциированный массив с цифрами
                $cifra = array('0' => true, '1' => true, '2' => true, '3' => true, '4' => true,
                    '5' => true, '6' => true, '7' => true, '8' => true, '9' => true);

                $upper_symb = array('A' => true, 'B' => true, 'C' => true, 'D' => true, 'E' => true, 'F' => true, 'G' => true, 'H' => true, 'I' => true, 'J' => true, 'K' => true, 'L' => true, 'M' => true, 'N' => true, 'O' => true, 'P' => true, 'Q' => true, 'R' => true,
                    'S' => true, 'T' => true, 'U' => true, 'V' => true, 'W' => true, 'X' => true, 'Y' => true, 'Z' => true, 'А' => true, 'Б' => true, 'В' => true, 'Г' => true, 'Д' => true, 'Е' => true, 'Ё' => true, 'Ж' => true, 'З' => true, 'И' => true,
                    'Й' => true, 'К' => true, 'Л' => true, 'М' => true, 'Н' => true, 'О' => true, 'П' => true, 'Р' => true, 'С' => true, 'Т' => true, 'У' => true, 'Ф' => true, 'Х' => true, 'Ц' => true, 'Ч' => true, 'Ш' => true, 'Щ' => true, 'Ъ' => true,
                    'Ы' => true, 'Ь' => true, 'Э' => true, 'Ю' => true, 'Я' => true);

                $lower_symb = array('a' => true, 'b' => true, 'c' => true, 'd' => true, 'e' => true, 'f' => true, 'g' => true, 'h' => true, 'i' => true, 'j' => true, 'k' => true, 'l' => true, 'm' => true, 'n' => true, 'o' => true, 'p' => true, 'q' => true, 'r' => true,
                    's' => true, 't' => true, 'u' => true, 'v' => true, 'w' => true, 'x' => true, 'y' => true, 'z' => true, 'а' => true, 'б' => true, 'в' => true, 'г' => true, 'д' => true, 'е' => true, 'ё' => true, 'ж' => true, 'з' => true, 'и' => true,
                    'й' => true, 'к' => true, 'л' => true, 'м' => true, 'н' => true, 'о' => true, 'п' => true, 'р' => true, 'с' => true, 'т' => true, 'у' => true, 'ф' => true, 'х' => true, 'ц' => true, 'ч' => true, 'ш' => true, 'щ' => true, 'ъ' => true,
                    'ы' => true, 'ь' => true, 'э' => true, 'ю' => true, 'я' => true);

                // вводим переменные для хранения информации о:
                $cifra_amount = 0; // количество цифр в тексте
                $word = ''; // текущее слово
                $words = array(); // список всех слов
                $znaki = 0;
                $kol_lower = 0;
                $kol_upper = 0;

                for ($i = 0; $i < strlen($text); $i++) // перебираем все символы текста
                {
//                    echo iconv('utf-8', 'cp1251', $text[$i]);
//                    echo $text[$i];
                    if (array_key_exists($text[$i], $cifra)) // если встретилась цифра
                        $cifra_amount++; // увеличиваем счетчик цифр
                    // если в тексте встретился пробел или текст закончился

//                    if(preg_match('/[a-zёа-я]/u', $text)){
//                        $kol_lower++;
//                    }
//
//                    if(preg_match('/[A-ZЁА-Я]/u', $text)){
//                        $kol_upper++;
//                    }
                    $char_count = count_chars(trim(iconv('utf-8', 'cp1251', $text)), 1);
                    foreach ($char_count as $symbol => $count) {
//                        if (array_key_exists(iconv("cp1251", "utf-8", $text[$i]), $lower_symb)) {
//                            $kol_lower++; // увеличиваем счетчик цифр
//                        }
//                        if (array_key_exists(iconv("cp1251", "utf-8", $text[$i]), $upper_symb)) {
//                            $kol_upper++; // увеличиваем счетчик цифр
//                        }
//                    }

                        if (array_key_exists(iconv('cp1251', 'utf-8', chr($symbol)), $lower_symb)) {
                            $kol_lower+=$count;
                        }
                        if (array_key_exists(iconv('cp1251', 'utf-8', chr($symbol)), $upper_symb)) {
                            $kol_upper+=$count;
                        }
                    }

                    if (ctype_punct($text[$i])) {
                        $znaki++;
                    }

                    if ($text[$i] == ' ' || $i == strlen($text) - 1) {
                        if ($word) // если есть текущее слово
                        {
                            // если текущее слово сохранено в списке слов
                            if (isset($words[$word]))
                                $words[$word]++; // увеличиваем число его повторов
                            else
                                $words[$word] = 1; // первый повтор слова
                        }
                        $word = ''; // сбрасываем текущее слово
                    } else // если слово продолжается
                        $word .= $text[$i]; //добавляем в текущее слово новый символ
                }
                ksort($words, SORT_STRING);
                // выводим количество цифр в тексте
                echo 'Количество цифр: ' . $cifra_amount . '<br>';
                // выводим количество слов в тексте
                echo 'Количество слов: ' . count($words) . '<br>';
                echo 'Количество знаков препинания: ' . $znaki . '<br>';
                echo 'Количество заглавных букв: ' . $kol_lower . '<br>';
                echo 'Количество строчных букв: ' . $kol_upper-1 . '<br>';
                echo 'Количество букв: ' . $kol_upper + $kol_lower-1 . '<br>';
                $char_count = count_chars(strtolower(trim(iconv('utf-8', 'cp1251', $text))), 1);

                echo '<h3>Символы</h3>';
                foreach ($char_count as $symbol => $count) {
                    if ($count > 0) {
                        echo 'Число символов ' . iconv('cp1251', 'utf-8', chr($symbol)) . ':  ' . $count . '<br>';
                    }
                }

                echo '<h3>Слова</h3>';
                foreach ($words as $oneword => $count) {
                    echo 'Число слов "' . $oneword . '":  ' . $count . '<br>';
                }
            }

            test_it($text);
            ?>
        </div>
    </div>
    <div style="display: flex; justify-content: center">
        <a href="index.html">
            <button>Другой анализ</button>
        </a>
    </div>

</main>
</body>
</html>
