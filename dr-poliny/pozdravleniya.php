<!DOCTYPE html>
<html lang="ru">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>Polina, s dr</title>
</head>
<body style="background: aliceblue">
<div class="infobg">
    <div class="info">
        <img src="images/close.png" alt="close" class="close_info">
        <div id="content" style="text-align: center">

        </div>
    </div>
</div>
<header>
    <nav>
        <a href="fanfik.php">Фанфик</a>
        <a style="cursor: url('/images/cursor1.cur'), auto; color: rgb(14,27,121)">Поздравления</a>
        <a href="photos.php">Фоточки</a>
    </nav>
    <div style="text-align: center; margin-top: 3em">
        <h1>Поздравляем с днём рождения!</h1>
    </div>
</header>
<div style="display: flex; justify-content: center" class="outer">
    <div style="width: 100%; display: flex; justify-content: center; top: 55%;" class="inner">
        <table class="pozdr">
            <tr>
                <td>
                    <div>
                        <img src="/images/Vitya1.jpg" class="vitya">
                    </div>
                </td>
                <td>
                    <div>
                        <img src="/images/sonia1.jpg" class="Sonia">
                    </div>
                </td>
                <td>
                    <div>
                        <img src="/images/artem1.jpg" class="Artem">
                    </div>
                </td>
                <td>
                    <div>
                        <img src="/images/zig-zag1.jpg" class="NastiaZ">
                    </div>
                </td>
                <td>
                    <div>
                        <img src="/images/lilia1.jpg" class="Lilia">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <img src="/images/leha1.jpg" class="Leha">
                    </div>
                </td>
                <td>
                    <div>
                        <img src="/images/nastiak1.jpg" class="NastiaK">
                    </div>
                </td>
                <td>
                    <div>
                        <img src="/images/sglypa1.jpg" class="sglypa">
                    </div>
                </td>
                <td>
                    <div>
                        <img src="/images/katia1.jpg" class="Katia">
                    </div>
                </td>
                <td>
                    <div>
                        <img src="/images/dasha1.jpg" class="Dasha">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <button style="cursor: url('/images/cursor2.cur'), auto; position: absolute; top: 90%; left: 93%; transform: translate(-50%, -50%); background: rgba(176, 196, 222,0.81); border-radius: 3px; border: #5100af 2px solid" onclick="href()">Дальше <i class="fa-sharp fa-solid fa-right-long"></i></button>
</div>
<script src="pozdravleniya.js"></script>
</body>
</html>