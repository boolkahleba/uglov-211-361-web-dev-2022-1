<!DOCTYPE html>
<html lang="ru">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>Polina, s dr</title>
</head>
<body style="background-image: url('/images/police.png'); height: 100vh">
<script src="hrefs.js"></script>
<div style="height: 100vh; background: rgba(54,1,49,0.4)">
    <div style="display: flex; justify-content: center">
        <div style="display: flex; justify-content: center" class="outer">
            <div style="display: flex;flex-direction: column;" class="inner">
                <button class="active" id="first" type="button" onclick="first()">
                    Нажми меня
                </button>
                <button class="passive" id="second" type="button" onclick="seccond()">
                    <i class="fa-solid fa-up-long"></i> Нажми кнопку <i class="fa-solid fa-up-long"></i>
                </button>
                <button class="passive" id="third" type="button" onclick="third()">
                    <i class="fa-solid fa-up-long"></i> Нажми кнопку <i class="fa-solid fa-up-long"></i>
                </button>
                <button class="passive" id="fourth" type="button" onclick="fourth()">
                    <i class="fa-solid fa-up-long"></i> Нажми кнопку <i class="fa-solid fa-up-long"></i>
                </button>
                <button class="passive" id="fifth" type="button" onclick="fifth()">
                    <i class="fa-solid fa-up-long"></i> Нажми кнопку <i class="fa-solid fa-up-long"></i>
                </button>
            </div>
        </div>
    </div>
</div>
</body>
</html>