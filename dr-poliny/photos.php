<!DOCTYPE html>
<html lang="ru">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>Polina, s dr</title>
</head>
<body style="background: aliceblue; height: 100vh">
<header>
    <nav>
        <a href="fanfik.php">Фанфик</a>
        <a href="pozdravleniya.php">Поздравления</a>
        <a  style="cursor: url('/images/cursor1.cur'), auto; color: rgb(14,27,121)">Фоточки</a>
    </nav>
</header>
<h1 style="text-align: center; margin-top: 1em;" title="Попробуй навести курсор на все и узнать послание!">Лучшие демотиваторы</h1>
<div style="text-align: right; margin-right: 5em"><button onclick="fv()" style="border: none; background: none; color: #5100af; font-size: 1.2em">К людям <i class="fa-sharp fa-solid fa-right-long"></i></button></div>
<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/demotivators/1.jpg" title="Полина!">
    </div>
    <div class="col-3">
        <img src="images/demotivators/2.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/3.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/4.jpg">
    </div>

</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/demotivators/5.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/6.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/7.jpg" title="с">
    </div>
    <div class="col-3">
        <img src="images/demotivators/8.jpg">
    </div>
</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/demotivators/9.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/10.jpg" title="днём">
    </div>
    <div class="col-3">
        <img src="images/demotivators/11.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/12.jpg">
    </div>>
</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/demotivators/13.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/14.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/15.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/16.jpg" title="рождения!">
    </div>
</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/demotivators/17.jpg" title="Расти">
    </div>
    <div class="col-3">
        <img src="images/demotivators/18.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/19.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/20.jpg">
    </div>
</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/demotivators/21.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/22.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/23.jpg" title="большой">
    </div>
    <div class="col-3">
        <img src="images/demotivators/24.jpg">
    </div>
</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/demotivators/25.jpg" title="не">
    </div>
    <div class="col-3">
        <img src="images/demotivators/26.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/27.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/28.jpg">
    </div>
</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/demotivators/29.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/30.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/31.jpg" title="будь">
    </div>
    <div class="col-3">
        <img src="images/demotivators/32.jpg">
    </div>
</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/demotivators/33.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/34.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/35.jpg">
    </div>
    <div class="col-3">
        <img src="images/demotivators/36.jpg" title="лапшой :)">
    </div>
</div>

<script>
    function fv(){
        setTimeout('document.location.href="photos2.php";', 100);
    }
</script>
</body>
</html>