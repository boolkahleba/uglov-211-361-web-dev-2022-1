<!DOCTYPE html>
<html lang="ru">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>Polina, s dr</title>
</head>
<body style="background-image: url('images/fon1.jpg'); background-size: 100%; height: 100vh">
<header>
    <nav>
        <a  style="cursor: url('/images/cursor1.cur'), auto; color: rgb(14,27,121)">Фанфик</a>
        <a href="pozdravleniya.php">Поздравления</a>
        <a href="photos.php">Фоточки</a>
    </nav>
</header>
<!--<div class="ffbg">-->
<!--    <div class="ff">-->
<!--        <p>-->
<!--            Утреннее солнце пыталось проникнуть своим светом в комнату, чуть освещая ее сквозь закрытые шторы. Девушка не могла поверить, что уже наступило утро, а птицы во всю щебетали за окном, создавая легкий шум. Полина откинулась на спинку стула, устало потирая затекшие шею и пальцы рук. Всю ночь она вновь сидела без сна, кропотливо рисуя портрет своего кумира. Она уже давно носила в себе эту идею, которую наконец смогла воплотить в жизнь: приближалось шестое декабря, а это значит, что скоро ее любимый исполнитель будет отмечать День Рождения. По мнению девушки, портрет был отличным подарком, особенно учитывая, сколько сил и любви она вложила в него.</p><p>-->
<!---->
<!--            Девушка давно знала этого приметного юношу: он был известен, имел много денег, а также обладал огромной харизмой. Именно это, а еще и его песни привлекли Полину. Она могла засыпать и просыпаться под его музыку, ходить с ней в наушниках целыми днями только ради того, чтобы наслаждаться его мелодичным голосом. Многие люди привыкли считать, что он бездарь, который не умеет петь. Возможно, в чем-то они и были правы, но песни этого парня всегда трогали девушку за душу. С тех самых пор, как она впервые услышала их, Даня Милохин поразил ее в самое сердце.</p><p>-->
<!--            <br><br>-->
<!--            ***-->
<!--            <br><br>-->
<!--        </p><p>Утро наступило быстро, и первое, что смогла сделать Полина, это опубликовать свой новый рисунок Дани в социальной сети. Она имела некоторую популярность среди его фанатов, которые уже не раз отмечали ее мастерство и точность передачи эмоций. Девушка знала, что скоро получит свою привычную порцию лайков и комментариев, а потом и парочку репостов в популярные фан-аккаунты.</p><p>-->
<!--<br><br>-->
<!--            Как бы то ни было, она просто хотела порадовать парня, даже если он не узнает об этом. Сам факт сделанного ею подарка доставлял ей удовольствие; сам процесс был ей безумно интересен.</p><p>-->
<!--<br><br>-->
<!--            Так и прошел её привычный день, во время которого она смогла побывать на тренировке, а затем и посидеть в кафе со своими подругами. Они были очень близки, как сестры, поэтому Полина любила проводить с ними время. Так, за неспешными разговорами и громким смехом проходил вечер.</p><p>-->
<!--<br><br>-->
<!--            -Ты сейчас ахуеешь,- сказала Соня, почти что родная сестра Полины,- я говорю тебе, ты будешь в полном шоке!-->
<!--<br><br>-->
<!--            Рыжая девушка выглядела взволнованной, удивленной и очень-очень радостной. Полина не понимала, в чем заключалось такой состояние подруги, поэтому сразу же поинтересовалась этим. В этот момент кто-то из других девушек заметил, в чем была причина такой суматохи, и все громко ахнули.-->
<!--<br><br>-->
<!--            -Очуметь,- хором сказали Катя и Насти.-->
<!--<br><br>-->
<!--            Полина до сих пор не видела, что же такого заметили девушки в телефоне рыжей, поэтому быстро повернула его к себе. На экране красовалась история Дани Милохина, в которую он выложил ее рисунок, а также написал: «Лучший портрет, что я видел! Спасибо @apolivanina!!».-->
<!--<br><br>-->
<!--            Девушка не верила своим глазам. Даня заметил её? Да даже в самом лучшем своем сне она не могла ожидать такого исхода. Наверное, это был просто сон.-->
<!--<br><br>-->
<!--            -Все не может быть настолько хорошо…- проговорила она,- вероятно, я слишком устала, иначе я не могу объяснить этот пиздец.-->
<!--<br><br>-->
<!--            ***-->
<!--<br><br>-->
<!--            Усталость после прогулки давала о себе знать, поэтому Полина поспешила закончить все вечерние дела, чтобы поскорее лечь спать. Погладив Киви и Молли, своих любимых домашних питомцев, она направилась в комнату. Стоило её голове коснуться подушки, как на её телефон пришло новое оповещение. Жмуря глаза от яркого света экрана, девушка посмотрела, что же потревожили её в столь поздний час.-->
<!--<br><br>-->
<!--            Даня Милохин (01:01)-->
<!--            Хочу еще раз сказать спасиба за ресунок, мне он очень понравился :)-->
<!--<br><br>-->
<!--            апол (01:01)-->
<!--            да не за что, все ради любимого исполнителя ;)-->
<!--<br><br>-->
<!--            Даня Милохин (01:01)-->
<!--            Для меня большая честь что кто-то рисует меня так красиво, я не ожидал, что будет так похоже-->
<!--            <br><br>-->
<!--            апол (01:02)-->
<!--            ну я очень рада, что тебе понравилось, с прошедшим днем рождения-->
<!--            <br><br>-->
<!--            Даня Милохин (01:02)-->
<!--            Ты инстересуешься только рисованием?-->
<!--            <br>-->
<!--            Так и началась их затяжная беседа, длиною в несколько месяцев. В начале это было похоже на простую дружескую беседу, но вскоре Полина начала замечать намеки, сигналы и прочие нелепые действия, которые она диктовала как знаки внимания со стороны блогера. Она не знала, что чувствует к этому странному, немного глупому, но чертовски интересному парню.-->
<!--            <br>-->
<!--            ***-->
<!--            <br>-->
<!--            Даня Милохин (14:04)-->
<!--            Скоро будет концерт в Кемерово, ты приедешь? Я оставлю для тебя и твоих друзей билеты )))-->
<!--            <br>-->
<!--            апол (14:08)-->
<!--            конечно, правда я сама планировала их купить, но раз уж ты их дашь просто так, я не могу отказаться-->
<!--            <br>-->
<!--            Даня Милохин (14:10)-->
<!--            Конечно, ты получишь их бесплатно! Точно приходи, я тебя не разочарую-->
<!--            <br>-->
<!--            апол (14:11)-->
<!--            тогда буду ждать)-->
<!--            <br>-->
<!--            ***-->
<!--            <br>-->
<!--            -Чёрт, тут такая толпа малолеток, - проговорила Соня, расталкивая мешающуюся шпану локтям, - ради тебя, сестра, я готова их потерпеть, не идти же тебя сюда одной!-->
<!--            <br>-->
<!--            -Да-да, спасибо, сестра, - проговорила Полина, которая листала мемы и явно прослушала все, что сказала ей подруга.-->
<!--            <br>-->
<!--            Время начала неумолимо приближалось, а в месте с ним нарастало и волнение внутри девушки: они никогда не виделись в живую, каков он будет при первой встрече? Она уже предвкушала этот момент.-->
<!--            <br>-->
<!--            Занятая своими мыслями, она и не заметила то, как прозвучали первые гитарные струны. В воздухе звучат звуки барабанов, а после слышится голос Дани, который любил петь с ухмылкой на его красивом лице. Его щёки немного покраснели, а также от чрезмерной жары он вытирал пот со лба своей рукой. Тем не менее, его выступление было прекрасным. Когда песня закончилась, публика начала кричать и хлопать. Затем Милохин решил представиться публике:-->
<!--            <br>-->
<!--            -Для всех тех, кто оказался здесь случайно и не знает меня, я - Даня Милохин, надеюсь, вы получите удовольствие от концерта, - проговорил он, вытирая рукой пот со лба.-->
<!--            <br>-->
<!--            В зале послышатся громкий девичий визг, от которого буквально закладывало уши, а Даня продолжил петь остальные свои песни.-->
<!--            <br>-->
<!--            Перед одной из песен, Даня остановился и направил серьезный взгляд в зал:-->
<!--            <br>-->
<!--            -Эти две песни я посвящаю своей дорогой подруге, своей опоре, той, кто заставляет мое сердце биться чаще.-->
<!--            <br>-->
<!--            Прозвучали первые строчки песни «Лапки».-->
<!--            <br>-->
<!--            ***-->
<!--            <br>-->
<!--            Исполняя следующую песню, Даня будто бы приобрел серьёзные настрой, невооруженным глазом можно было увидеть, насколько сильно он волнуется. Зал заполнили первые аккорды неизвестной песни.-->
<!--            <br>-->
<!--            Строки песни тронули девушку до глубины души. Она знала, что это поется про неё. Не про Валю Карнавал, Айм Катюшу или Юлю Гаврилину, а именно про нее. В этот момент она была счастливее всех на свете.-->
<!---->
<!--        </p>-->
<!--    </div>-->
<!--</div>-->

<div class="row" style="margin: 0">
    <div class="col-1" style="background-image: url('images/fon1.jpg'); background-size: 100%"></div>
    <div class="col-10 ff">
        <p>Утреннее солнце пыталось проникнуть своим светом в комнату, чуть освещая ее сквозь закрытые шторы. Девушка не могла поверить, что уже наступило утро, а птицы во всю щебетали за окном, создавая легкий шум. Полина откинулась на спинку стула, устало потирая затекшие шею и пальцы рук. Всю ночь она вновь сидела без сна, кропотливо рисуя портрет своего кумира. Она уже давно носила в себе эту идею, которую наконец смогла воплотить в жизнь: приближалось шестое декабря, а это значит, что скоро ее любимый исполнитель будет отмечать День Рождения. По мнению девушки, портрет был отличным подарком, особенно учитывая, сколько сил и любви она вложила в него.</p>
            <br>
        <p>Девушка давно знала этого приметного юношу: он был известен, имел много денег, а также обладал огромной харизмой. Именно это, а еще и его песни привлекли Полину. Она могла засыпать и просыпаться под его музыку, ходить с ней в наушниках целыми днями только ради того, чтобы наслаждаться его мелодичным голосом. Многие люди привыкли считать, что он бездарь, который не умеет петь. Возможно, в чем-то они и были правы, но песни этого парня всегда трогали девушку за душу. С тех самых пор, как она впервые услышала их, Даня Милохин поразил ее в самое сердце.</p>
            <br>
            <p style="text-align: center">***<p>
            <br>
        <p>Утро наступило быстро, и первое, что смогла сделать Полина, это опубликовать свой новый рисунок Дани в социальной сети. Она имела некоторую популярность среди его фанатов, которые уже не раз отмечали ее мастерство и точность передачи эмоций. Девушка знала, что скоро получит свою привычную порцию лайков и комментариев, а потом и парочку репостов в популярные фан-аккаунты.</p>
            <br>
        <p>Как бы то ни было, она просто хотела порадовать парня, даже если он не узнает об этом. Сам факт сделанного ею подарка доставлял ей удовольствие; сам процесс был ей безумно интересен.</p>
            <br>
        <p>Так и прошел её привычный день, во время которого она смогла побывать на тренировке, а затем и посидеть в кафе со своими подругами. Они были очень близки, как сестры, поэтому Полина любила проводить с ними время. Так, за неспешными разговорами и громким смехом проходил вечер.</p>
            <br>
        <p>-Ты сейчас ахуеешь,- сказала Соня, почти что родная сестра Полины,- я говорю тебе, ты будешь в полном шоке!</p>
            <br>
        <p>Рыжая девушка выглядела взволнованной, удивленной и очень-очень радостной. Полина не понимала, в чем заключалось такой состояние подруги, поэтому сразу же поинтересовалась этим. В этот момент кто-то из других девушек заметил, в чем была причина такой суматохи, и все громко ахнули.</p>
            <br>
        <p>-Очуметь,- хором сказали Катя и Насти.</p>
            <br>
        <p>Полина до сих пор не видела, что же такого заметили девушки в телефоне рыжей, поэтому быстро повернула его к себе. На экране красовалась история Дани Милохина, в которую он выложил ее рисунок, а также написал: «Лучший портрет, что я видел! Спасибо @apolivanina!!».</p>
            <br>
        <p>Девушка не верила своим глазам. Даня заметил её? Да даже в самом лучшем своем сне она не могла ожидать такого исхода. Наверное, это был просто сон.</p>
            <br>
        <p>-Все не может быть настолько хорошо…- проговорила она,- вероятно, я слишком устала, иначе я не могу объяснить этот пиздец.</p>
            <br>
        <p style="text-align: center">***</p>
            <br>
        <p>Усталость после прогулки давала о себе знать, поэтому Полина поспешила закончить все вечерние дела, чтобы поскорее лечь спать. Погладив Киви и Молли, своих любимых домашних питомцев, она направилась в комнату. Стоило её голове коснуться подушки, как на её телефон пришло новое оповещение. Жмуря глаза от яркого света экрана, девушка посмотрела, что же потревожили её в столь поздний час.</p>
            <br>
        <p>Даня Милохин (01:01)</p>
        <p>Хочу еще раз сказать спасиба за ресунок, мне он очень понравился :)</p>
            <br>
        <p>апол (01:01)</p>
        <p>да не за что, все ради любимого исполнителя ;)</p>
            <br>
        <p>Даня Милохин (01:01)</p>
        <p> Для меня большая честь, что кто-то рисует меня так красиво. Я не ожидал, что будет так похоже</p>
            <br>
        <p>апол (01:02)</p>
        <p>ну я очень рада, что тебе понравилось, с прошедшим днем рождения</p>
            <br>
        <p>Даня Милохин (01:02)</p>
        <p>Ты инстересуешься только рисованием?</p>
            <br>
        <p>Так и началась их затяжная беседа, длиною в несколько месяцев. В начале это было похоже на простую дружескую беседу, но вскоре Полина начала замечать намеки, сигналы и прочие нелепые действия, которые она диктовала как знаки внимания со стороны блогера. Она не знала, что чувствует к этому странному, немного глупому, но чертовски интересному парню.</p>
            <br>
        <p style="text-align: center">***</p>
            <br>
        <p>Даня Милохин (14:04)</p>
        <p>Скоро будет концерт в Кемерово, ты приедешь? Я оставлю для тебя и твоих друзей билеты )))</p>
            <br>
        <p>апол (14:08)</p>
        <p>конечно, правда я сама планировала их купить, но раз уж ты их дашь просто так, я не могу отказаться</p>
            <br>
        <p>Даня Милохин (14:10)</p>
        <p>Конечно, ты получишь их бесплатно! Точно приходи, я тебя не разочарую</p>
            <br>
        <p>апол (14:11)</p>
        <p>тогда буду ждать)</p>
            <br>
        <p style="text-align: center; margin: 0; padding: 0">***</p>
            <br>
        <p>-Чёрт, тут такая толпа малолеток, - проговорила Соня, расталкивая мешающуюся шпану локтям, - ради тебя, сестра, я готова их потерпеть, не идти же тебя сюда одной!</p>
            <br>
        <p>-Да-да, спасибо, сестра, - проговорила Полина, которая листала мемы и явно прослушала все, что сказала ей подруга.</p>
            <br>
        <p>Время начала неумолимо приближалось, а в месте с ним нарастало и волнение внутри девушки: они никогда не виделись в живую, каков он будет при первой встрече? Она уже предвкушала этот момент.</p>
            <br>
        <p>Занятая своими мыслями, она и не заметила то, как прозвучали первые гитарные струны. В воздухе звучат звуки барабанов, а после слышится голос Дани, который любил петь с ухмылкой на его красивом лице. Его щёки немного покраснели, а также от чрезмерной жары он вытирал пот со лба своей рукой. Тем не менее его выступление было прекрасным. Когда песня закончилась, публика начала кричать и хлопать. Затем Милохин решил представиться публике:</p>
            <br>
        <p>-Для всех тех, кто оказался здесь случайно и не знает меня, я - Даня Милохин, надеюсь, вы получите удовольствие от концерта, - проговорил он, вытирая рукой пот со лба.</p>
            <br>
        <p>В зале послышатся громкий девичий визг, от которого буквально закладывало уши, а Даня продолжил петь остальные свои песни.
            <br>
        <p>Перед одной из песен, Даня остановился и направил серьезный взгляд в зал:</p>
            <br>
        <p>-Эти две песни я посвящаю своей дорогой подруге, своей опоре, той, кто заставляет мое сердце биться чаще.</p>
            <br>
        <p>Прозвучали первые строчки песни «Лапки».</p>
            <br>
        <p style="text-align: center">***</p>
            <br>
        <p>Исполняя следующую песню, Даня будто бы приобрел серьёзный настрой, невооруженным глазом можно было увидеть, насколько сильно он волнуется. Зал заполнили первые аккорды неизвестной песни.</p>
            <br>
        <div style="display: flex; justify-content: center">
            <div>
        Свет. Озарил сегодня этот чудный<br>
        День. Твой день рождения сегодня 20<br>
        Лет. Тебя Полина, поздравляю я сейчас<br>
        Такой подруги я не мог и пожелать<br>
        <br>
        Мой тяжкий крест на праздник я прийти не смог<br>
                Но без таких друзей как ты я как без ног<br><br></div></div>
        <p>Строки песни тронули девушку до глубины души. Она знала, что это поется про неё. Не про Валю Карнавал, Айм Катюшу или Юлю Гаврилину, а именно про нее. В этот момент она была счастливее всех на свете.</p>
    </div>
    <div class="col-1" style="background-image: url('images/fon1.jpg'); background-size: 100%"></div>
</div>

</body>
</html>