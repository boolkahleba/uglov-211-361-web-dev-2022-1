<!DOCTYPE html>
<html lang="ru">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>Polina, s dr</title>
</head>
<body style="background: aliceblue; height: 100vh">
<header>
    <nav>
        <a href="fanfik.php">Фанфик</a>
        <a href="pozdravleniya.php">Поздравления</a>
        <a  style="cursor: url('/images/cursor1.cur'), auto; color: rgb(14,27,121)">Фоточки</a>
    </nav>
</header>
<h1 style="text-align: center; margin-top: 1em;" title="Попробуй навести курсор на все и узнать послание!">Лучшие люди</h1>
<div style="text-align: left; margin-left: 5em"><button onclick="fv()" style="border: none; background: none; color: #5100af; font-size: 1.2em"><i class="fa-sharp fa-solid fa-left-long"></i> К демотиваторам</button></div>
<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/unused/1.jpg" title="Полина!">
    </div>
    <div class="col-3">
        <img src="images/unused/2.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/3.jpg" title="В">
    </div>
    <div class="col-3">
        <img src="images/unused/4.jpg">
    </div>

</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/unused/5.jpg" title="одной">
    </div>
    <div class="col-3">
        <img src="images/unused/6.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/7.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/8.jpg" title="из">
    </div>

</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/unused/9.jpg" title="картинок">
    </div>
    <div class="col-3">
        <img src="images/unused/10.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/11.jpg" title="есть">
    </div>
    <div class="col-3">
        <img src="images/unused/12.jpg">
    </div>

</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/unused/13.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/36.jpg" title="пасхалка!" onclick="hr('pozdravlenie.txt')">
    </div>
    <div class="col-3">
        <img src="images/unused/15.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/16.jpg">
    </div>

</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/unused/17.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/18.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/19.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/20.jpg"title="Просто">
    </div>

</div>


<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/unused/14.jpg"title="нажми!">
    </div>
    <div class="col-3">
        <img src="images/unused/21.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/22.jpg" title="на">
    </div>
    <div class="col-3">
        <img src="images/unused/23.jpg" title="одно">
    </div>

</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/unused/24.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/25.jpg" title="из">
    </div>
    <div class="col-3">
        <img src="images/unused/26.jpg"title="фото">
    </div>
    <div class="col-3">
        <img src="images/unused/27.jpg">
    </div>

</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/unused/28.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/29.jpg"title="сделанных">
    </div>
    <div class="col-3">
        <img src="images/unused/30.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/31.jpg">
    </div>

</div>

<div class="row" style="margin: 1em 0 0;">
    <div class="col-3">
        <img src="images/unused/32.jpg"title="в">
    </div>
    <div class="col-3">
        <img src="images/unused/33.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/34.jpg">
    </div>
    <div class="col-3">
        <img src="images/unused/35.jpg"title="аэропорте">
    </div>

</div>

<script>
    function fv(){
        setTimeout('document.location.href="photos.php";', 100);
    }
    function hr(url) {
        const a = document.createElement('a')
        a.href = url
        a.download = url.split('/').pop()
        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
    }

</script>

</body>
</html>