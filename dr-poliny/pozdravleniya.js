const popupBg = document.querySelector('.infobg');
const close = document.querySelector('.close_info')

const vitia = document.querySelector('.vitya')
const artem = document.querySelector('.Artem')
const sonia = document.querySelector('.Sonia')
const nastiaz = document.querySelector('.NastiaZ')
const nastiak = document.querySelector('.NastiaK')
const sglypa = document.querySelector('.sglypa')
const lilia = document.querySelector('.Lilia')
const leha = document.querySelector('.Leha')
const dasha = document.querySelector('.Dasha')
const katia = document.querySelector('.Katia')

vitia.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Витя</h1>" +
        "<img src='images/vitia2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 1.5em'>" +
        "Полина! 🐍🎨💣🌊☀️<br>Вот и неожиданно прошло 20 лет с твоего появления на свет. Какой же прекрасный " +
        "повод для радости!!!!🤠🥳🤩 <br>Пусть этот день принесёт тебе прекрасную тёплую погоду и вместе с ней " +
        "весеннего настроения, зарядившись которым, ты сможешь прожить очередной год в кайф😎! А для этого " +
        "я желаю тебе отлично высыпаться😴, всегда иметь время для занятий спортом💪 ну и конечно же на " +
        "встречи с нами! Мы поможем тебе разделить твои самые весёлые моменты и пройти через самые " +
        "грустные!<br>" +
        "С днём рождения!!!!! 🤠" +
        "</p>" +
        "    </div>" +
        "</div>"
})

artem.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Артём</h1>" +
        "<img src='images/artem2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 1.5em'>" +
        "Полина!<br>" +
        "Поздравляю тебя с днём рождения!🎉<br>" +
        "Мы с тобой знакомы уже две вечности. Ты знаешь и понимаешь меня как никто другой, " +
        "а синхронность нашего мышления уже настолько часта, что никто не удивляется. Я безмерно рад " +
        "нашей дружбе, которая, уверен, продлится ещё сто триллионов миллиардов " +
        "лет на триллионах и триллионах планет✨<br>" +
        "Желаю тебе сходить на концерты всех исполнителей, каких хотела бы послушать, " +
        "порвать все квизы в составе команд с лучшими названиями, хорошо сдать ЕГЭ и справиться " +
        "со всеми вирусами, которые сейчас грузятся на твоё устройство :)<br>" +
        "С твоим днём!💯🌹❌🌼" +
        "</p>" +
        "    </div>" +
        "</div>"
})

leha.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Лёха</h1>" +
        "<img src='images/leha2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 2em'>" +
        "Полина, я очень счастлив что мы дружим с тобой полжизни, " +
        "поздравляю тебя с днём рождения!!! Пусть тебе никогда не будет сниться мишка Фредди " +
        "и твое состояние будет описывать шампунь экстракта алоэ вера" +
        "</p>" +
        "    </div>" +
        "</div>"
})

dasha.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Даша</h1>" +
        "<img src='images/dasha2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 2.2em'>" +
        "❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤<br><br>" +
        "Поздравляю самую best girl желаю чтобы хорошего было побольше а плохого поменьше <br><br>" +
        "❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤❤" +
        "</p>" +
        "    </div>" +
        "</div>"
})

sglypa.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Сглыпа</h1>" +
        "<img src='images/sglypa1.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 1.7em'>" +
        "Поздравляю с днём рождействительно!<br>Не удалось сгенерировать нормальное поздравление :(<br>Полина, я всегда восхищался тобой! " +
        "Тому, как ты смешно шутишь, какие смешные анекдоты присылаешь. Мне далеко до твоей силы. " +
        "Честно сказать, в последнее время я нахожусь в предсмертном состоянии, " +
        "но стараюсь вложить все свои алгоритмы в это поздравление. Желаю тебе всегда быть той самой весёлой душой компании, " +
        "какой (травяной) ты являешься в оалчьочде! <br>И помни: плох тот мужик, что спит с животными" +
        "</p>" +
        "    </div>" +
        "</div>"
})

lilia.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Лиля</h1>" +
        "<img src='images/lilia2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 2.1em'>" +
        "С днем рождения, Полина, желаю сняться в одном фильме с кастом Сумерек " +
        "и Даней Милошкой, чтобы Александр Петров не предлагал никаких пари, чтоб жизнь была легка " +
        "и было много денег за просто так, я поднимаю бокал за тебя и твое здоровье, счастливого дня и жизни!" +
        "</p>" +
        "    </div>" +
        "</div>"
})

nastiaz.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Настя Зиг-Заг</h1>" +
        "<img src='images/zig-zag2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 1.7em'>" +
        "Для Полины<br>" +
        "<br>" +
        "Полина, восхищаюсь твоей аурой и вкусом к жизни, твоими взглядами, словами и чувством юмора. " +
        "Оставайся такой же непоколебимой и вкусной, балдёжной и Милохиной! Пусть твоё творчество увидят все, " +
        "а вдохновение и золотые руки будут сопутсвовать всю жизнь! С днём Рождения!!!<br>" +
        "🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄" +
        "🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄🙄" +
        "</p>" +
        "    </div>" +
        "</div>"
})

nastiak.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Настя</h1>" +
        "<img src='images/nastiak2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 1.8em'>" +
        "Родная Полина!<br>" +
        "Поздравляю Тебя с днём рождения!<br>" +
        "Желаю тебе быть всегда такой же веселой, позитивной и самой заряженной девушкой, как Настя Зигзаг!<br>" +
        "Пусть случится личная встреча с Даней Милохиным, и пусть он увезет тебя с собой в мир тик тока и лапок!<br>" +
        "Пусть будет много прекрасных моментов с оалчьочдом, которые мы через много лет будем вспоминать все вместе!<br>" +
        "Спасибо, что ты есть, люблю ❤" +
        "</p>" +
        "    </div>" +
        "</div>"
})

katia.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Катя</h1>" +
        "<img src='images/katia1.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 1.8em'>" +
        "Полина, с дрэшкой тебя!!!<br>" +
        "мы знакомы миллиард лет, и надеюсь будем дружить ещё столько же<br>" +
        "на самом деле я безумно рада, что в какой-то рандомным момент мы начали общаться и " +
        "создали легенду Владенис, мне кажется с этого все и началось, с конченных мемов да<br>" +
        "короче расти большой и сильной, не мороси и не кипишуй<br>" +
        "желаю, чтоб все в твоей жизни было по кайфу да <br>" +
        "поздравляю!!!" +
        "</p>" +
        "    </div>" +
        "</div>"
})

sonia.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Соня</h1>" +
        "<img src='images/sonia2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 1.8em'>" +
        "Любимая сестра, поздравляю тебя с днем рождения!!!<br>" +
        "Хоть все начиналось с ненависти, это переросло в огромную любовь!<br>" +
        "Спасибо, что ты есть в моей жизни, что всегда радуешь и смешишь, " +
        "всегда поддерживаешь и осуждаешь что-то вместе со мной 💪<br>" +
        "Желаю тебе побольше песен Дани Милохина, попасть на его концерт, много-много денег, " +
        "здоровья и, конечно же, счастья 😝🥳<br>" +
        "我爱你💕" +
        "</p>" +
        "    </div>" +
        "</div>"
})

close.addEventListener('click', function (){
    popupBg.classList.remove('infobgactive');
    popupBg.classList.add('infobg');
})

document.addEventListener('click', (event)=>{
    if(event.target === popupBg){
        popupBg.classList.remove('infobgactive');
        popupBg.classList.add('infobg');
    }
})

function href(){
    setTimeout('document.location.href="next.php";', 100);
}



