let a = 0
let up = "<i class=\"fa-solid fa-up-long\"></i> Нажми кнопку <i class=\"fa-solid fa-up-long\"></i>"
let down = "<i class=\"fa-solid fa-down-long\"></i> Нажми кнопку <i class=\"fa-solid fa-down-long\"></i>"

function first() {
    if (a == 0) {
        document.getElementById('first').innerHTML = down
        document.getElementById('second').innerHTML = "Нажми меня"
        document.getElementById('third').innerHTML = up
        document.getElementById('fourth').innerHTML = up
        document.getElementById('fifth').innerHTML = up
        a = 1
        download('mainer.txt')
    }
}

function seccond() {
    if (a == 1) {
        document.getElementById('first').innerHTML = down
        document.getElementById('second').innerHTML = down
        document.getElementById('third').innerHTML = "Нажми меня"
        document.getElementById('fourth').innerHTML = up
        document.getElementById('fifth').innerHTML = up
        a = 2
        download('troyan.txt')
    }
}

function third() {
    if (a == 2) {
        document.getElementById('first').innerHTML = down
        document.getElementById('second').innerHTML = down
        document.getElementById('third').innerHTML = down
        document.getElementById('fourth').innerHTML = "Нажми меня"
        document.getElementById('fifth').innerHTML = up
        a = 3
        download('virus.txt')
    }
}

function fourth() {
    if (a == 3) {
        document.getElementById('first').innerHTML = down
        document.getElementById('second').innerHTML = down
        document.getElementById('third').innerHTML = down
        document.getElementById('fourth').innerHTML = down
        document.getElementById('fifth').innerHTML = "Нажми меня"
        a = 4
        download('fishing.txt')
    }
}

function fifth() {
    if (a == 4) {
        document.getElementById('first').innerHTML = "С днём роджения!!!"
        document.getElementById('second').innerHTML = "С днём роджения!!!"
        document.getElementById('third').innerHTML = "С днём роджения!!!"
        document.getElementById('fourth').innerHTML = "С днём роджения!!!"
        document.getElementById('fifth').innerHTML = "С днём роджения!!!"
        setTimeout('document.location.href="pozdravleniya.php";', 2000);
        a = 5
    }
}

function download(url) {
    const a = document.createElement('a')
    a.href = url
    a.download = url.split('/').pop()
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
}