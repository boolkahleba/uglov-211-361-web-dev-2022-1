const popupBg = document.querySelector('.infobg');
const close = document.querySelector('.close_info')

const milohin = document.querySelector('.milohin')
const kristof = document.querySelector('.kristof')
const makelove = document.querySelector('.makelove')
const kaplan = document.querySelector('.kaplan')
const jecob = document.querySelector('.jecob')

kaplan.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Юра Каплан</h1>" +
        "<img src='images/kaplan2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 2em'>" +
        "С днём рождения, дорогая!<br>С этим ясным весенним днём!<br>Мы в семнадцатом году порвали<br>" +
        "Мы в две тыщи двадцать третьем всех порвём!<br>" +
        "Пускай буржуй-эксплуататор знает<br>" +
        "Что мы будем стоять до конца!<br>" +
        "Развивается красное знамя<br>" +
        "В наших красных сердцах!<br>" +
        "В наших красных сердцах!<br><br>🎁✨🎊🎉💥🎁✨🎊🎉💥" +
        "</p>" +
        "    </div>" +
        "</div>"
})

milohin.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Даня Милохин</h1>" +
        "<img src='images/milohin2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 2em'>" +
        "🐾💪🏋‍♀🍑❌👫💋<br><br>" +
        "Полина!<br>" +
        "Поздравляю тебя с днём рождения. Пусть у тебя всё в жизни будет с кайфом! И пускай у меня нет лапок, я всё равно постараюсь радовать тебя своими песнями каждый день✨<br>" +
        "<br>🐾💪🏋‍♀🍑❌👫💋" +
        "</p>" +
        "    </div>" +
        "</div>"
})

jecob.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Джейкоб Блэк</h1>" +
        "<img src='images/jecob2.jfif' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 1.6em'>" +
        "Полина, поздравляю тебя от лица членов всей нашей стаи 🐺<br>" +
        "Для меня это честь быть тем, кто сможет передать все наши искренние пожелания для тебя!<br>" +
        "Желаем тебе искреннего счастья, много денег (чтобы хватило на дом, как у Калленов), крепкого здоровья, как настоящего оборотня-Квилета💪, ну и простого исполнения мечтаний! Ради тебя, я готов стать альфой нашего клана!\n" +
        "Я бы мог написать и больший текст, но моя линька не дает мне покоя, поэтому мне пора уходить. Надеюсь, нам доведется познакомиться ближе 🐺" +
        "</p>" +
        "    </div>" +
        "</div>"
})

kristof.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Кристофф</h1>" +
        "<img src='images/kristof2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 2em'>" +
        "Полина, поздравляю тебя!<br>" +
        "Желаю тебе всего самого прекрасного! Ты восхитительная девушка, превосходящая своим обоянием даже Свена!✨ Приглашаю тебя в Долину Живых Камней отпраздновать эту знаменательную дату вместе с моей семьёй🤗<br>" +
        "Буду очень рад тебя видеть!<br>До встречи)🥕❄" +
        "</p>" +
        "    </div>" +
        "</div>"
})

makelove.addEventListener('click', function (){
    popupBg.classList.remove('infobg');
    popupBg.classList.add('infobgactive');
    document.getElementById('content').innerHTML = "<div class='row'><div class='col-6'><h1>Глеб Лысенко</h1>" +
        "<img src='images/makelove2.jpg' style='width: 100%'>" +
        "        </div>" +
        "    <div class='col-6' style='text-align: justify; padding-top: 3em'>" +
        "        <p style='font-size: 1.7em'>" +
        "я устал от холодов, но наконец пришла весна, что значит пора просыпаться. но наступила не только весна, " +
        "ведь настал и твой день рождения! сегодня только твой день, и я поздравляю тебя с ним, Полина!<br>" +
        "будь всегда такой крутой, никогда не печалься, ведь грусть стреляет всегда в упор, ты знаешь. короче, " +
        "мечтай о великом! не полетишь ты, если рождён был ползать, но от гусениц до бабочек всего лишь один кокон, помни! " +
        "а теперь я снова жду год, чтобы написать поздравление самой клёвой девчонке!" +
        "</p>" +
        "    </div>" +
        "</div>"
})

close.addEventListener('click', function (){
    popupBg.classList.remove('infobgactive');
    popupBg.classList.add('infobg');
})

document.addEventListener('click', (event)=>{
    if(event.target === popupBg){
        popupBg.classList.remove('infobgactive');
        popupBg.classList.add('infobg');
    }
})

function href(){
    setTimeout('document.location.href="pozdravleniya.php";', 100);
}