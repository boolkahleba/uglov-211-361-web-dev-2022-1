<!DOCTYPE html>
<html lang="ru">
<head>
    <link href="style.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&family=Noto+Sans:wght@300&display=swap"
          rel="stylesheet">
    <script src="https://kit.fontawesome.com/44de4fd467.js" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <title>Углов Артём Евгеньевич, 211-361, ЛР9-В4</title>
</head>

<body>
<header>
    <img src="images/Ресурс%202.png" height="100px">
    <p>Углов Артём Евгеньевич, 211-361, ЛР9-В4</p>
</header>

<main>

    <div style="text-align: center; font-size: 0.7em; line-height: 0.7em">
        <h2>Вычисленные значения функции</h2>
    </div>
    <div style="display: flex; justify-content: center">
        <div>
            <?php
            $x = 3;    // начальное значение аргумента
            $encounting = 10;    // количество вычисляемых значений
            $step = 2;    // шаг изменения аргумента
            $type = 'E';    // тип верстки
            $min_value = -300;    // минимальное значение, останавливающее вычисления
            $max_value = 300;    // максимальное значение, останавливающее вычисления
            $max = '';
            $min = '';
            $avg = '';
            $sum = 0;

            switch ($type) {
                case 'B':
                    echo '<ul>';
                    break;
                case 'C':
                    echo '<ol>';
                    break;
                case 'D':
                    echo '<table>';
                    echo '<tr><td>N</td><td>x</td><td>f(x)</td></tr>';
                    break;
                case 'E':
                    echo '<div style="width: 100%">';
            }

            // цикл с заданным количеством итераций
            for ($i = 0; $i < $encounting; $i++, $x += $step) {
                if ($x == 5) {
                    $f = "error";
                } elseif ($x <= 10) {   // если аргумент меньше или равен 10

                    $f = (5 - $x) / (1 - $x / 5);    // вычисляем функцию
                } elseif ($x < 20 && $x > 10) {    // если аргумент меньше 20
                    $f = $x * $x / 4 + 7;    // вычисляем функцию
                } else {
                    $f = 2 * $x - 21;    // вычисляем функцию
                }

                if ($f != "error") {
                    if ($f >= $max_value || $f <= $min_value) {    // если вышли за рамки диапазона
                        break;    // закончить цикл досрочно
                    }

                    $f = round($f, 3);
                    if ($i == 0) {
                        $max = $f;
                        $min = $f;
                    }
                    $sum += $f;

                    if ($f > $max) {
                        $max = $f;
                    }
                    if ($f < $min) {
                        $min = $f;
                    }
                    $avg = round($sum / $encounting, 3);
                }

                switch ($type) {
                    case 'A':
                        echo 'f(' . $x . ')=' . $f;    // выводим аргумент и значение функции
                        if ($i < $encounting - 1) // если это не последняя итерация цикла
                            echo '<br>';
                        break;
                    case 'C':
                    case 'B':
                        echo '<li>f(' . $x . ')=' . $f . '</li>';
                        break;
                    case 'D':
                        echo '<tr><td>' . ($i + 1) . '. </td><td>' . ($x) . ' </td><td> ' . $f . '</td></tr>';
                        break;
                    case 'E':
                        echo '<div class="red">f(' . $x . ')=' . $f . '</div>';
                }
            }


            switch ($type) {
                case 'B':
                    echo '</ul>';
                    break;
                case 'C':
                    echo '</ol>';
                    break;
                case 'D':
                    echo '</table>';
                    break;
                case 'E':
                    echo '</div>';
            }
            ?>
        </div>
    </div>
    <div style="display: flex; justify-content: center; padding-top: 2em">
        <?php
        echo 'Максимальное значение: ' . $max . '<br>';
        echo 'Минимальное значение: ' . $min . '<br>';
        echo 'Среднее значение: ' . $avg . '<br>';
        echo 'Сумма: ' . $sum;
        ?>
    </div>
</main>
</body>

<footer>
    <div style="text-align: center">
        <?php
        echo 'type = ' . $type;
        ?>
    </div>
</footer>
</html>