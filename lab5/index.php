<!DOCTYPE html>
<html lang="en">
<head>
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <!--    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&family=Noto+Sans:wght@300&display=swap"-->
    <!--          rel="stylesheet">-->
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>Uglov 211-361 lab5</title>
</head>
<body>

<main style="text-align: center; padding-top: 1em">
    <h1 style="color: darkslategrey">Боевики</h1>

    <h2>Форсаж</h2>
    <div style="display: flex; justify-content: center; padding-bottom: 1em">
        <ul style="text-align: left">
            <?php
            $mass = ["Боевик, триллер, криминал", "США, Германия, 2001 год", "Длительность: 106 минут."];
            foreach ($mass as $item) {
                echo '<li>' . $item . '</li>';
            }
            ?>
        </ul>
    </div>
    <div>
        <?php
        $image_forsaje = 'images/forsaje_poster.jpg';
        $jown_wick = 'images/jown_wick.jpg';
        ?>

        <img src="<?php echo($image_forsaje); ?>" alt="Форсаж" style="width: 23em;"  class="image-in-text">
    </div>
    <div style="display: flex; justify-content: center">
        <div class="text-block">
            <p>
                Практически каждый автолюбитель в городе знаком с командой Доминика Торетто, что организовывает
                специальные
                гонки, а победители таких соревнования довольно часто превращаются в его соратников и активно участвуют
                в
                преступных делах. Они совершают угоны и ограбления, не оставляя следов, так что сотрудникам полиции
                невероятно сложно поймать их на деле. Сам Доминик не раз становился чемпионом самых сложных уличных
                гонок и
                прекрасно разбирается в скоростных автомобилях. Недавно к нему пожаловал молодой парень Брайан, желающий
                поучаствовать в одном из соревнований и показать все, на что способен.<br>

                Новичок безумно любит машины и является поклонником быстрой езды, чем и покоряет Доминика. Он старается
                войти к нему в доверие, ведь такая задача стоит перед решительным сотрудником полиции, что вынужден
                работать
                под прикрытием. Его руководство подозревает Дома и его команду в грабежах трейлеров, но доказательства
                найти
                сложно. Новенький участник всеми силами пытается выполнить задание, но постепенно проникается симпатией
                к
                гонщикам и лидеру группы, в том числе. Он начинает понимать их увлечения, образ жизни и даже совершенные
                преступления не кажутся такими чудовищными. Он еще не предполагает, что в скором времени ему придется
                выбирать между новыми друзьями и долгом полицейского.
            </p>
        </div>
    </div>
    <div style="display: flex; justify-content: center; padding-bottom: 2em">
        <a href="images_page.php?name=<?php $type="Форсаж"; echo $type ?>" style="color: #222222; text-decoration: none">
            <div class="button">
                <p>Смотреть ещё изображения</p>
            </div>
        </a>
    </div>

    <h2>Джон Уик</h2>
    <div style="display: flex; justify-content: center; padding-bottom: 1em">
        <ul style="text-align: left">
            <li>Боевик, триллер.</li>
            <li>США, Китай, 2014 год.</li>
            <li>Длительность: 101 минута.</li>
        </ul>
    </div>
    <div>
        <img src="<?php echo($jown_wick); ?>" alt="Джон Уик" style="width: 20em;"  class="image-in-text">
    </div>
    <div style="display: flex; justify-content: center">
        <div class="text-block">
            <p>
                Джон Уик, бывший киллер. Совсем недавно герой овдовел. От жены остался только щенок Дейзи. Потеряв
                любимую, он полюбил маленького пса, ведь умершая жена так была к нему привязана. Однажды вдовец
                повстречал члена русской банды — Иосифа, который пожелал купить его автомобиль. Но для героя эта машина
                связана с воспоминаниями о Хелен и он не может ее продать. Тогда бандиты подкараулили Джона, избили его,
                а нового друга Дейзи застрелили, и просто угнали автомобиль. Уик выясняет, что его ограбил сын главаря
                русской мафии, на которого работал герой. Он намерен уберечь парня от мести и просит героя не трогать
                сына. Тот честно говорит, что отныне месть убийце щенка стала смыслом его жизни. Мафиози посылает своих
                людей убить Джона. Но герою нечего больше терять и, поэтому он становится неуязвим и отчаянно
                бесстрашен. Он убивает всю группу и скрывается в отеле "Континенталь". Однако авторитет узнает о его
                местонахождении отдает своим бандитам приказ пристрелить Джона. Что же случится на этот раз?
            </p>
        </div>
    </div>
    <div style="display: flex; justify-content: center; padding-bottom: 2em">
        <a href="images_page.php?name=<?php $type="Джон Уик"; echo $type ?>" style="color: #222222; text-decoration: none">
            <div class="button">
                <p>Смотреть ещё изображения</p>
            </div>
        </a>
    </div>
</main>
</body>
<footer>
    <div id="contacts">
        <a href="https://t.me/Boolka_hlebaa" target="_blank" target="_blank"><i class="fa-brands fa-telegram"></i></a>
        <a href="https://vk.com/boolka_hleba" target="_blank" target="_blank"><i class="fa-brands fa-vk"></i></a>
        <a style="font-size: 0.5em; color: black; text-decoration: none" href="tel: +7(923)613-90-55">+7 (923)
            613-90-55</a>
    </div>
</footer>
</html>