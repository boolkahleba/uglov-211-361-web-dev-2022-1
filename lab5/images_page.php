<!DOCTYPE html>
<html lang="en">
<head>
    <link href="style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com" rel="preconnect">
    <link crossorigin href="https://fonts.gstatic.com" rel="preconnect">
    <!--    <link href="https://fonts.googleapis.com/css2?family=Merriweather:wght@300&family=Noto+Sans:wght@300&display=swap"-->
    <!--          rel="stylesheet">-->
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <meta charset="UTF-8">
    <title>Uglov 211-361 lab3</title>
</head>
<body>
<header>
    <div style="padding-top: 2em" id="top">
        <nav>
            <a href="index.php">
                На главную страницу
            </a>
        </nav>
    </div>
</header>
<?php
$name = $_GET['name'];

include "db.php";
if ($name=="Форсаж"){
    $result = mysqli_query($mysql, "SELECT * FROM `images` WHERE film='Форсаж'");
} else if ($name=="Джон Уик"){
    $result = mysqli_query($mysql, "SELECT * FROM `images` WHERE film='Джон Уик'");
}

?>


<main style="text-align: center; padding-top: 1em">
    <h1 style="color: darkslategrey"><?php echo $name ?></h1>

    <div>
        <?php
        while ($name = mysqli_fetch_assoc($result)) {
            ?>
            <div>
                <img title="<?php echo $name['name']; ?>" src="images/<?php echo $name['img']; ?>" class="image-in-text" style="width: 60%; "/>
            </div>
            <?php
        }
        ?>
    </div>

    <div style="display: flex; justify-content: center; padding-bottom: 2em">
        <a href="#top" style="color: #222222; text-decoration: none">
            <div class="button">
                <p>Подняться наверх</p>
            </div>
        </a>
    </div>

</main>
</body>
<footer>
    <div id="contacts">
        <a href="https://t.me/Boolka_hlebaa" target="_blank" target="_blank"><i class="fa-brands fa-telegram"></i></a>
        <a href="https://vk.com/boolka_hleba" target="_blank" target="_blank"><i class="fa-brands fa-vk"></i></a>
        <a style="font-size: 0.5em; color: black; text-decoration: none" href="tel: +7(923)613-90-55">+7 (923) 613-90-55</a>
    </div>
</footer>
</html>