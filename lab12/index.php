<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Bootstrap</title>
    <link href="style.css" rel="stylesheet">
    <script crossorigin="anonymous" src="https://kit.fontawesome.com/44de4fd467.js"></script>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>
<body>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4"
        crossorigin="anonymous"></script>
<main>
    <div class="row" style="padding-top: 2em; background-color: darkslateblue; margin-right: 0">
        <div class="col-md-8">
            <div class="heading">
                <h1>Брендинг магазина сладостей</h1>
                <p>
                    Проект за 2 семестр
                </p>

            </div>
        </div>

        <div class="col-md-6" style="margin-bottom: 2em; text-align: center">
            <img src="images/sladosti.jpg" style="height: 15em; border: 1px white solid">
        </div>
    </div>

    <div class="row" style="padding-top: 2em; background-color: white; margin-right: 0">
        <div class="col-md-4" style="padding-left: 5em">
            <h2>Описание проекта</h2>
            <p>

                Мы помогаем жителям городов, в которых нет возможности приобрести
                оригинальные сладости. Наш магазин с возможностью доставки во все города страны позволит покупателям
                найти желанные лакомства и провести чаепитие по-новому. Но начиная работать над этой идеей мы
                столкнулись с проблемой низких продаж, которая возникла из-за малой известности нашего магазина. В связи
                с этим, создание бренда стало нашим приоритетом.
            </p>
            <p>
                Особенности проекта:
                <br>
                Результатом проекта стало формирование стиля магазина. Разработанный дизайн сайта по правилам
                UI/UX(главная страница, логотип, каталог, карточка товара, категорийная страница), пробные свёрстанные
                страницы на хостинге мосполитеха.
            </p>
            <p>
                Преимущества проекта:
            <br>
                Готовая вёрстка страниц и написание технического задания позволяют добиться таких положительных эффектов
                как: экономия времени и средств заказчика, а готовый бизнес-план, позволяет
                предполагать финансовые затраты и прибыль и прогнозировать будущее магазина.
            </p>
        </div>
        <div class="col-md-6" style="text-align: center">
            <img src="images/logo.jpg" style="height: 20em">
        </div>
    </div>
    <div class="row" style="padding-top: 2em; background-color: darkslateblue; margin-right: 0">
        <div class="col-md-6" style="text-align: center">
            <h1 style="text-align: right; color: white">Контакты</h1>
        </div>
    </div>
    <div class="row" style="padding-top: 2em; background-color: darkslateblue; margin-right: 0">
        <div class="col-md-6" style="text-align: center; padding-left: 5em; color: white; margin-bottom: 3em">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Your name:">
            </div>
            <div class="form-group" style="padding-top: 2em">
                <input type="email" class="form-control" placeholder="Your email:">
            </div>
            <div class="form-group" style="padding-top: 2em">
                <textarea class="form-control" rows="8" placeholder="Your message:"></textarea>
            </div>
            <div style="padding-top: 2em">
                <button type="submit" class="btn btn-primary">Отправить</button>
            </div>
        </div>
        <div class="col-md-6">
            <div>

                <p>
                    <a style="font-size: 2em; color: black; text-decoration: none; padding-top: 2em"
                       href="https://t.me/Boolka_hlebaa" target="_blank"><img src="images/tg.png"
                                                                              style="height: 1.5em">Boolka_hlebaa</a>
                </p>
                <p style="padding-top: 2em">
                    <a style="font-size: 2em; color: black; text-decoration: none; padding-top: 2em"
                       href="https://vk.com/boolka_hleba" target="_blank"><img src="images/vk.png"
                                                                               style="height: 1.5em">Boolka_hlebaa</a>
                </p>
                <p style="padding-top: 2em">
                    <a style="font-size: 2em; color: black; text-decoration: none"
                       href="tel: +7(923)613-90-55"><img src="images/phone.png" style="height: 1.5em"> +7 (923)
                        613-90-55</a>
                </p>

                <p style="padding-top: 2em">
                    <a style="font-size: 2em; color: black; text-decoration: none"
                       href="mailto: artiomuglov@yandex.ru"><img src="images/post.png" style="height: 1.5em">
                        artiomuglov@yandex.ru</a>
                </p>

            </div>
        </div>
    </div>

</main>
</body>
</html>