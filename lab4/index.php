<?php
include 'header.html';
?>
<main>
    <div style="display: flex; justify-content: center; padding: 2em">
        <div class="form" style="width: 18em">

            <?php
            if(isset($_GET['N'])){
                $name=$_GET['N'];
                $email=$_GET['E'];
                $source=$_GET['S'];
            }
            ?>


            <form action="home.php" method="post">
                <p style="font-size: 1.5em"><b>Обратная связь</b></p><br>

                <p style="font-size: 0.7em"><strong>ФИО:</strong>
                    <input type="text" value="<?php echo ($name)?>" maxlength="25" size="23em" name="name"></p><br>

                <p style="font-size: 0.7em"><strong>Email:</strong>
                    <input type="text" value="<?php echo ($email)?>" maxlength="25" size="20em" name="email"></p><br>

                <p style="font-size: 0.7em"><strong>Откуда Вы узнали о нас?</strong>

                <p style="font-size: 0.5em"><input type="radio" name="source" value="znakomie"
                        <?php if ($source=="znakomie") echo (" checked ")?>>От знакомых</p>
                <p style="font-size: 0.5em"><input type="radio" name="source" value="sotseti"
                        <?php if ($source=="sotseti") echo (" checked "); echo ($source)?>>В соцсетях</p>
                <p style="font-size: 0.5em"><input type="radio" name="source" value="statia"
                        <?php if ($source=="statia") echo (" checked ")?>>Из статьи</p>

                <select size="1", style="width: 9em", name="category">
                    <option disabled>Тип обращения</option>
                    <option value="jaloba">Жалоба</option>
                    <option value="propose" selected>Предложение</option>
                </select>

                <p style="font-size: 0.7em">Комментарий<br>
                    <textarea name="message" cols="40" rows="5"></textarea>
                </p>
                <p style="font-size: 0.5em">
                    <input type="file", name="attachment"></p><br>
                <p style="font-size: 0.5em"><input type="checkbox", name="agree">Даю согласие на обработку персональных данных
                </p><br>
                <input type="submit">
            </form>

        </div>
    </div>
</main>
</body>
</html>