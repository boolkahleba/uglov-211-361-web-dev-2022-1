function power() {
    let a = document.getElementById("pwr_a").value;
    let k = document.getElementById("pwr_k").value;
    let result = 1;
    for (let i=0; i<k; i++){
        result = result * a;
    }
    if (k<1){result = "k должно быть натуральным!"}
    document.getElementById("pwr").innerHTML = "Ответ: "+result;
}

function NOD(){
    let a = document.getElementById("nod_a").value;
    let b = document.getElementById("nod_b").value;
    while (a!=0 && b!=0){
        if (a>=b){
            a %= b;
        }else {
            b %= a;
        }
    }
    a=Number(a)+Number(b);
    document.getElementById("nod").innerHTML = "Ответ: " + a;
}

function smallest_num(){
    let a = document.getElementById("smln_a").value;
    let arr = a.split('');
    a=10;
    for (let i=0; i<arr.length;i++){
        if(a>arr[i]){
            a=arr[i];
        }
    }
    document.getElementById("smln").innerHTML = "Ответ: " + a;
}

function pluralization(){
    let a = document.getElementById("plur_a").value;
    let arr = a.split('');
    let b = arr[arr.length-1];
    let d = " было найдено ";
    if (b==1){
        let g="запись";
        d = " была найдена ";
    }else if (b==2 || b==3 || b==4){
        g="записи";
    }else {g="записей";}
    b = arr[arr.length-2]
    if (b==1){g="записей";d = " было найдено ";}
    document.getElementById("plur").innerHTML = "В результате выполнения запроса" + d + a+ " " + g;
}

function fib() {
    let a = document.getElementById("fib_a").value;
    let result = 0;
    if (a==1){result = 0;}
    else if (a==2||a==3){result = 1;}
    else if (a>3){
        let num1 = 1;
        result = 1;
        for (let i=3; i<a; i++){
            result = result +num1;
            num1 = result - num1;
        }

    }else {result="Неправильный ввод";}
    document.getElementById("fib").innerHTML = "Ответ: "+result;
}